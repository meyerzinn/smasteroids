module github.com/20zinnm/smasteroids

require (
	github.com/akavel/rsrc v0.0.0-20170831122431-f6a15ece2cfd // indirect
	github.com/faiface/glhf v0.0.0-20181018222622-82a6317ac380 // indirect
	github.com/faiface/mainthread v0.0.0-20171120011319-8b78f0a41ae3 // indirect
	github.com/faiface/pixel v0.8.1-0.20190121221830-9e0f11abbb30
	github.com/go-gl/gl v0.0.0-20181026044259-55b76b7df9d2 // indirect
	github.com/go-gl/glfw v0.0.0-20181213070059-819e8ce5125f // indirect
	github.com/go-gl/mathgl v0.0.0-20180804195959-cdf14b6b8f8a // indirect
	github.com/gobuffalo/packr v1.21.9
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/jakecoffman/cp v0.1.0
	github.com/mitchellh/gox v0.4.0 // indirect
	github.com/mitchellh/iochan v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	golang.org/x/image v0.0.0-20181116024801-cd38e8056d9b
)
